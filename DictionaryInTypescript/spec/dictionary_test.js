"use strict";
describe("Dictionary", function () {
    describe("String as key", function () {
        var dict;
        beforeEach(function () {
            dict = new Collections.Dictionary();
        });
        describe("Add", function () {
            it("Should add an element", function () {
                // Arrange
                var key = "key1";
                var value = "value1";
                // Act
                dict.add(key, value);
                // Assert
                expect(dict.count()).toBe(1);
                expect(dict.keys()).toEqual([key]);
                expect(dict.values()).toEqual([value]);
            });
            it("Should throw an error trying to add an element with 'undefined' as key", function () {
                // Act and Assert
                expect(function () { dict.add(undefined, "value1"); }).toThrow();
                expect(dict.count()).toBe(0);
            });
            it("Should throw an error trying to add an element with an empty string as key", function () {
                // Act and Assert
                expect(function () { dict.add("", "value1"); }).toThrow();
                expect(dict.count()).toBe(0);
            });
            it("Should throw an error trying to add an element with a key that is already in the dictionary", function () {
                // Arrange
                var key = "key1";
                dict.add(key, "value1");
                // Act and Assert
                expect(function () { dict.add(key, "value2"); }).toThrow();
                expect(dict.count()).toBe(1);
            });
        });
        describe("Remove", function () {
            it("Should remove the pair associated to the given key", function () {
                // Arrange
                var keys = ["key1", "keyToRemove", "key2"];
                var values = ["value1", "valueToRemove", "value2"];
                for (var i = 0; i < keys.length; i++) {
                    dict.add(keys[i], values[i]);
                }
                // Act
                var pairHasBeenRemoved = dict.remove("keyToRemove");
                // Assert
                expect(pairHasBeenRemoved).toBe(true);
                expect(dict.count()).toBe(2);
                expect(dict.keys()).toEqual(["key1", "key2"]);
                expect(dict.values()).toEqual(["value1", "value2"]);
            });
            it("Should NOT remove the element associated to the given key, since there is no element with the given key", function () {
                // Arrange
                var keys = ["key1", "key2", "key3"];
                var values = ["value1", "value2", "value3"];
                for (var i = 0; i < keys.length; i++) {
                    dict.add(keys[i], values[i]);
                }
                // Act
                var pairHasBeenRemoved = dict.remove("keyToRemove");
                // Assert
                expect(pairHasBeenRemoved).toBe(false);
                expect(dict.count()).toBe(3);
                expect(dict.keys()).toEqual(keys);
                expect(dict.values()).toEqual(values);
            });
            it("Should throw an error trying to remove an element with 'undefined' as key", function () {
                // Arrange
                var keys = ["key1", "key2"];
                var values = ["value1", "value2"];
                for (var i = 0; i < keys.length; i++) {
                    dict.add(keys[i], values[i]);
                }
                // Act and Assert
                expect(function () { dict.remove(undefined, "value1"); }).toThrow();
                expect(dict.count()).toBe(2);
            });
        });
        describe("GetValue", function () {
            beforeEach(function () {
                var keys = ["key1", "key2", "key3"];
                var values = ["value1", "value2", "value3"];
                for (var i = 0; i < keys.length; i++) {
                    dict.add(keys[i], values[i]);
                }
            });
            it("Should throw an error trying to get the value of an 'undefined' key", function () {
                // Act and Assert
                expect(function () { dict.getValue(undefined); }).toThrow();
            });
            it("Should get the value associated to a given key", function () {
                // Act
                var retreivedValue = dict.getValue("key3");
                // Assert
                expect(retreivedValue).toBe("value3");
            });
            it("Should return null trying to get the value associated to a non existing key", function () {
                // Act
                var retreivedValue = dict.getValue("nonExistingKey");
                // Assert
                expect(retreivedValue).toBe(null);
            });
        });
        describe("ContainsKey", function () {
            beforeEach(function () {
                var keys = ["key1", "key2"];
                var values = ["value1", "value2"];
                for (var i = 0; i < keys.length; i++) {
                    dict.add(keys[i], values[i]);
                }
            });
            it("Should throw an error trying to check if the dictionary contains an 'undefined' key", function () {
                // Act and Assert
                expect(function () { dict.containsKey(undefined); }).toThrow();
            });
            it("Should find that the given key is contained in the dictionary", function () {
                // Act
                var containsTheKey = dict.containsKey("key2");
                // Assert
                expect(containsTheKey).toBe(true);
            });
            it("Should find that the given key is NOT contained in the dictionary", function () {
                // Act
                var containsTheKey = dict.containsKey("nonExistingKey");
                // Assert
                expect(containsTheKey).toBe(false);
            });
        });
        describe("ChangeValueForKey", function () {
            beforeEach(function () {
                var keys = ["key1", "key2", "key3", "key4"];
                var values = ["value1", "value2", "value3", "value4"];
                for (var i = 0; i < keys.length; i++) {
                    dict.add(keys[i], values[i]);
                }
            });
            it("Should throw an error trying to change the value associated to an 'undefined' key", function () {
                // Act and Assert
                expect(function () { dict.changeValueForKey(undefined, "newValue"); }).toThrow();
            });
            it("Should throw and error trying to change the value associated to a NON existing key", function () {
                // Act and Assert
                expect(function () { dict.changeValueForKey("nonExistingKey"); }).toThrow();
            });
            it("Should change the value associated to a given key", function () {
                // Arrange
                var newValue = "newValue";
                var keyOfThePairToWhichChangeTheValue = "key3";
                // Act
                dict.changeValueForKey(keyOfThePairToWhichChangeTheValue, newValue);
                // Assert
                expect(dict.getValue(keyOfThePairToWhichChangeTheValue)).toBe(newValue);
                expect(dict.values()).toEqual(["value1", "value2", "newValue", "value4"]); // this check is only to be sure that all other values are unchanged
            });
        });
    });
});
//# sourceMappingURL=dictionary_test.js.map